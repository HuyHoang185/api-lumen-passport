<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TrackingmoreController extends Controller
{
    public function getTrackingRequest()
    {
        require_once('./track.class.php/Init.class.php');
        \APITracking\Api::setApiKey("d4d79fe7-e87e-4de7-a933-29e0f53423ce");
        return Http::get('https://api.trackingmore.com');
    }

    public function postTrackingRequest()
    {
       return $response = \APITracking\Courier::get();
    }

    public function detect()
    {
        $response = \APITracking\Detect::post("RU123456789CN");
    }
}
